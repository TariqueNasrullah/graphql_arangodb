package db_service

import (
	"context"
	"fmt"
	"github.com/arangodb/go-driver"
	ahttp "github.com/arangodb/go-driver/http"
	dataloader "github.com/graph-gophers/dataloader/v6"
	"log"
)

type Book struct {
	Id       string `json:"_key"`
	Name     string `json:"name"`
	AuthorId string `json:"authorid"`
}

type Author struct {
	Id      string   `json:"_key"`
	Name    string   `json:"name"`
	BooksId []string `json:"booksid"`
}

func ConnectToDatabase(dbName string) driver.Database {
	conn, err := ahttp.NewConnection(ahttp.ConnectionConfig{
		Endpoints: []string{"http://localhost:8529"},
	})
	if err != nil {
		log.Fatalf("Can't connect to arangodb err: %v\n", err)
	}
	c, err := driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication("root", "admin"),
	})
	if err != nil {
		log.Fatalf("Can't create arangodb client err: %v\n", err)
	}
	// Opening database _system
	db, err := c.Database(nil, dbName)
	if err != nil {
		log.Fatalf("Can't open database err: %v\n", err)
	}
	return db
}

func GetBook(db driver.Database, id string) Book {
	query := "FOR book in books FILTER book._key == @id RETURN book"
	bindVars := map[string]interface{}{
		"id": id,
	}
	fmt.Println("querying book from db")
	cursor, err := db.Query(nil, query, bindVars)
	if err != nil {
		log.Fatalf("Can't get book\nerr: %v", err)
	}
	defer cursor.Close()
	var book Book
	cursor.ReadDocument(nil, &book)
	return book
}

func GetAllBooks(db driver.Database) []Book {
	query := "FOR book IN books RETURN book"
	cursor, err := db.Query(nil, query, nil)
	if err != nil {
		log.Fatalf("Query error\nerr: %v\n", err)
	}
	defer cursor.Close()

	var books []Book
	for {
		var bookDocument Book
		_, err := cursor.ReadDocument(nil, &bookDocument)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatalf("Can't fetch book documnet\nerr: %v\n", err)
		}
		books = append(books, bookDocument)
	}
	return books
}

func InsertBook(db driver.Database, book Book) {
	col, err := db.Collection(nil, "books")
	if err != nil {
		log.Fatalf("Can't open books collection\nerr: %v", err)
	}
	_, err = col.CreateDocument(nil, book)
	if err != nil {
		log.Fatalf("Can't create book document\nerr: %v", err)
	}
}

func GetAuthor(db driver.Database, id string) Author {
	query := "FOR author in authors FILTER author._key == @id RETURN author"
	bindVars := map[string]interface{}{
		"id": id,
	}
	cursor, err := db.Query(nil, query, bindVars)
	if err != nil {
		log.Fatalf("Can't get author\nerr: %v", err)
	}
	defer cursor.Close()
	var author Author
	cursor.ReadDocument(nil, &author)
	return author
}

func GetAllAuthors(db driver.Database) []Author {
	query := "FOR book IN authors RETURN book"
	cursor, err := db.Query(nil, query, nil)
	if err != nil {
		log.Fatalf("Query error\nerr: %v\n", err)
	}
	defer cursor.Close()

	var authors []Author
	for {
		var authorDocument Author
		_, err := cursor.ReadDocument(nil, &authorDocument)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatalf("Can't fetch book documnet\nerr: %v\n", err)

		}
		authors = append(authors, authorDocument)
	}
	return authors
}

func UpdateAuthor(db driver.Database, authorId string, updatedBookList []string) {
	col, err := db.Collection(nil, "authors")
	if err != nil {
		log.Fatalf("Can't read authors collection\nerr: %v", err)
	}
	patch := map[string]interface{}{
		"booksid": updatedBookList,
	}
	_, err = col.UpdateDocument(nil, authorId, patch)
	if err != nil {
		log.Fatalf("Can't update auhor document\nerr: %v", err)
	}
}

// Query multiple books
func GetBooks(ids []string) []Book {
	db := ConnectToDatabase("_system")
	query := "FOR book IN books FILTER book._key IN @bookIds RETURN book"
	bindVars := map[string]interface{} {
		"bookIds": ids,
	}
	fmt.Println("querying books from db with data loader")
	cursor, err := db.Query(nil, query, bindVars)
	if err != nil {
		log.Fatalf("Query error\nerr: %v\n", err)
	}
	defer cursor.Close()

	var books []Book
	for {
		var bookDocument Book
		_, err := cursor.ReadDocument(nil, &bookDocument)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatalf("Can't fetch book documnet\nerr: %v\n", err)
		}
		books = append(books, bookDocument)
	}
	return books
}

func BatchGetBookFunc(_ context.Context, keys dataloader.Keys) []*dataloader.Result {
	fmt.Printf("keys: %v", keys)
	var results []*dataloader.Result
	books := GetBooks(keys.Keys())
	for i, _  := range keys {
		results = append(results, &dataloader.Result{Data: books[i]})
	}
	return results
}
