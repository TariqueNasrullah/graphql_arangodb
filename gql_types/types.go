package gql_types

import (
	"context"
	"github.com/Shafin098/gql/db_service"
	"github.com/arangodb/go-driver"
	"github.com/graph-gophers/dataloader/v6"
	"github.com/graphql-go/graphql"
	"log"
)

func DefineTypes(db driver.Database) (*graphql.Object, *graphql.Object) {
	bookType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Book",
		Fields: graphql.Fields{
			"id":   &graphql.Field{Type: graphql.String},
			"name": &graphql.Field{Type: graphql.String},
		},
	})

	authorType := graphql.NewObject(graphql.ObjectConfig{
		Name: "Author",
		Fields: graphql.Fields{
			"id":   &graphql.Field{Type: graphql.String},
			"name": &graphql.Field{Type: graphql.String},
		},
	})

	bookType.AddFieldConfig("author", &graphql.Field{
		Type: authorType,
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			author := db_service.GetAuthor(db, p.Source.(db_service.Book).AuthorId)
			return author, nil
		},
	})

	authorType.AddFieldConfig("books", &graphql.Field{
		Type: graphql.NewList(bookType),
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			cache := &dataloader.NoCache{}
			loader := dataloader.NewBatchedLoader(db_service.BatchGetBookFunc, dataloader.WithCache(cache))

			bookThunks := make([]dataloader.Thunk, 0)
			authorsBooks := make([]db_service.Book, 0)
			author := p.Source.(db_service.Author)

			for _, authorsBookId := range author.BooksId {
				bookThunk := loader.Load(context.TODO(), dataloader.StringKey(authorsBookId))
				bookThunks = append(bookThunks, bookThunk)
			}
			for _, bookThunk := range bookThunks {
				book, err := bookThunk()
				if err != nil {
					log.Fatalf("Can't call book thunk\n err: %v", err)
				}
				authorsBooks = append(authorsBooks, book.(db_service.Book))
			}
			return authorsBooks, nil
		},
	})
	return bookType, authorType
}

func DefineRootQuery(db driver.Database, bookType *graphql.Object, authorType *graphql.Object) graphql.ObjectConfig {
	rootQuery := graphql.ObjectConfig{
		Name: "RootQuery",
		Fields: graphql.Fields{
			"author": &graphql.Field{
				Type: authorType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{Type: graphql.String},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					author := db_service.GetAuthor(db, p.Args["id"].(string))
					return author, nil
				},
			},
			"authors": &graphql.Field{
				Type: graphql.NewList(authorType),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					authors := db_service.GetAllAuthors(db)
					return authors, nil
				},
			},
			"book": &graphql.Field{
				Type: bookType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{Type: graphql.String},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					book := db_service.GetBook(db, p.Args["id"].(string))
					return book, nil
				},
			},
			"books": &graphql.Field{
				Type: graphql.NewList(bookType),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					books := db_service.GetAllBooks(db)
					return books, nil
				},
			},
		},
	}

	return rootQuery
}

func DefineMutation(db driver.Database, bookType *graphql.Object) graphql.ObjectConfig {
	mutation := graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"addBook": &graphql.Field{
				Type: bookType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"name": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"authorId": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					id := p.Args["id"].(string)
					name := p.Args["name"].(string)
					authorId := p.Args["authorId"].(string)
					newBook := db_service.Book{Id: id, Name: name, AuthorId: authorId}
					db_service.InsertBook(db, newBook)
					authors := db_service.GetAllAuthors(db)
					for i, author := range authors {
						if authorId == author.Id {
							authors[i].BooksId = append(author.BooksId, id)
							db_service.UpdateAuthor(db, authors[i].Id, authors[i].BooksId)
							break
						}
					}
					return newBook, nil
				},
			},
		},
	}

	return mutation
}
