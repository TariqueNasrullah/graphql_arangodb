package main

import (
	"fmt"
	"github.com/Shafin098/gql/db_service"
	"github.com/Shafin098/gql/gql_types"
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"log"
	"net/http"
)

const PORT = ":8080"

func CorsMiddleWare( handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Max-Age", "86400")
		w.Header().Set("Access-Control-Allow-Methods", "POST, PATCH, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, api_key, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		w.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		w.Header().Set("Access-Control-Allow-Credentials", "true")

		handler.ServeHTTP(w, r)
	})
}

func main() {
	db := db_service.ConnectToDatabase("_system")

	bookType, authorType := gql_types.DefineTypes(db)
	rootQuery := gql_types.DefineRootQuery(db, bookType, authorType)
	mutation := gql_types.DefineMutation(db, bookType)

	schemaConfig := graphql.SchemaConfig{
		Query:    graphql.NewObject(rootQuery),
		Mutation: graphql.NewObject(mutation),
	}
	schema, err := graphql.NewSchema(schemaConfig)
	if err != nil {
		log.Fatalf("failed to create new schema, error: %v", err)
	}

	graphqlHandler := handler.New(&handler.Config{
		Schema:   &schema,
		Pretty:   true,
		GraphiQL: true,
	})
	http.Handle("/graphql", CorsMiddleWare(graphqlHandler))
	fmt.Printf("Listening at port %s\n", PORT)
	log.Fatal(http.ListenAndServe(PORT, nil))
}
